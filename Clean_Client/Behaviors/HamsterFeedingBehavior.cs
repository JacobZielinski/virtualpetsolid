﻿using Clean_Client.Enum;
using Clean_Client.Interfaces;
using Clean_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Behaviors
{
    public class HamsterFeedingBehavior : IFeedingBehavior
    {
        public CurrentEffect Feed(Food currentFood)
        {
            if (currentFood.Type == FoodTypeEnum.CARROT)
            {
                return new CurrentEffect()
                {
                    EffectType = EffectTypeEnum.POISON,
                    Duration = 4,
                    Intensity = 3
                };
            }
            else return new CurrentEffect()
            {
                EffectType = EffectTypeEnum.FEED,
                Duration = 1,
                Intensity = 1,
            };
        }
    }
}
