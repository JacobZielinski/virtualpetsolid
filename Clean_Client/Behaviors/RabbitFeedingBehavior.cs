﻿using Clean_Client.Enum;
using Clean_Client.Interfaces;
using Clean_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Behaviors
{
    public class RabbitFeedingBehavior : IFeedingBehavior
    {
        public CurrentEffect Feed(Food currentFood)
        {
            if (currentFood.Type == FoodTypeEnum.CARROT)
            {
                return new CurrentEffect()
                {
                    EffectType = EffectTypeEnum.FEED,
                    Duration = 2,
                    Intensity = 2
                };
            }
            else return new CurrentEffect()
            {
                EffectType = EffectTypeEnum.FEED,
                Duration = 1,
                Intensity = 1,
            };
        }
    }
}
