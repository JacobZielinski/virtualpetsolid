﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Enum
{
    public enum EffectTypeEnum
    {
        POISON = 1,
        REST = 2,
        FEED = 3,
        SICK = 4,
    }
}
