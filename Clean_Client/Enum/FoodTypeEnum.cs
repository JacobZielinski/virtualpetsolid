﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Enum
{
    public enum FoodTypeEnum
    {
        CARROT = 1,
        MILK = 2,
        WATER = 3,
        GRASS = 4,
    }
}
