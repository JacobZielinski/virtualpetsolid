﻿using Clean_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client
{
    public class GameEngine
    {
        public static void UpdateHealth(Creature ourPet)
        {
            ourPet.Health -= ourPet.Tiredness;
            ourPet.Health -= ourPet.Hunger;
            foreach (var currentEffect in ourPet.EffectList.Where(x => x.CurrentIteration < x.Duration))
            {
                handleCurrentEffect(ourPet, currentEffect);
            }
            ourPet.EffectList.RemoveAll(x => x.CurrentIteration >= x.Duration);

        }
        private static void handleCurrentEffect(Creature ourPet, CurrentEffect currentEffect)
        {
            currentEffect.CurrentIteration++;
            switch (currentEffect.EffectType)
            {
                case Enum.EffectTypeEnum.FEED:
                    {
                        ourPet.Hunger -= currentEffect.Intensity;
                        if (ourPet.Hunger < 0)
                        {
                            ourPet.Hunger = 0;
                        }
                        ourPet.Health += currentEffect.Intensity;
                        break;
                    }
                case Enum.EffectTypeEnum.POISON:
                    {
                        ourPet.Health -= currentEffect.Intensity;
                        break;
                    }
            };
        }

        internal static void UpdateTirednessAndHunger(Creature ourPet)
        {
            //ourPet.Tiredness++;
            if (ourPet.Hunger < 5)
            {
                ourPet.Hunger++;
            }
        }

        internal static bool CheckIfGameIsOver(Creature ourPet)
        {
            if (ourPet.Health < 0)
            {
                return true;
            }
            return false;
        }
    }
}
