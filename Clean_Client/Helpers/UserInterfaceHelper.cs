﻿using Clean_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Helpers
{
    public class UserInterfaceHelper
    {
        public static string ParseCurrentEffectToString(CurrentEffect currentEffect)
        {
            return string.Format($"[{currentEffect.EffectType.ToString()}] Intensity: '{currentEffect.Intensity}' Duration: '{currentEffect.Duration}' CurrentIteration '{currentEffect.CurrentIteration}'");
        }
    }
}
