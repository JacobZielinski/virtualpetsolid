﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Clean_Client
{
    public abstract class ViewModelBase : DependencyObject, INotifyPropertyChanged
    {
        public ViewModelState currentState;


        protected ViewModelBase() { }

        public ViewModelState CurrentState { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public enum ViewModelState
        {
            NotInitialized = 0,
            Normal = 1,
            New = 2,
            Edit = 3,
            Delete = 4,
            List = 5,
            Properties = 6,
            Special = 7
        }
    }
}
