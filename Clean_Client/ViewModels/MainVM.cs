﻿using Clean_Client.Helpers;
using Clean_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Clean_Client.ViewModels
{
    class MainVM : ViewModelBase
    {

        // Do wyswietlenia:
        // Imie zwierzaka ( i jego rasa)
        // Stan Zdrowia
        // Stan wyspania
        // Current avatar (czyli jez z zamknietymi oczami, lub zywy)
        // Text view, gdzie mamy info co sie w goole dzieje (czyli zdechl, albo sie zatrul, cokolwiek)
        private Creature ourPet;
        public Creature OurPet
        {
            get
            {
                if (ourPet == null)
                {
                    MessageBox.Show("Program error, pet does not exists somehow!");
                }
                return ourPet;
            }
            set
            {
                ourPet = value;
                NotifyPropertyChanged("OurPet");
            }
        }
        public int Health
        {
            get
            {
                return OurPet.Health;
            }
            set
            {
                OurPet.Health = value;
                if (OurPet.Health < 0)
                {
                    TextStatus = "R.I.P";
                    MessageBox.Show("R.I.P");
                }
                NotifyPropertyChanged("Health");
            }
        }
        private int maxHealth;
        public int MaxHealth
        {
            get
            {
                return maxHealth;
            }
            set
            {
                this.maxHealth = value;
                NotifyPropertyChanged("MaxHealth");
            }
        }
        public int Hunger
        {
            get
            {
                return OurPet.Hunger;
            }
        }
        public string PetName
        {
            get
            {
                return string.Format($"--==[{OurPet.Name}]==--");
            }
        }

        private string textStatus = "Start game..";
        public string TextStatus
        {
            get
            {
                return textStatus;
            }
            set
            {
                textStatus = value;
                NotifyPropertyChanged("TextStatus");
            }
        }

        public string PetDetails
        {
            get
            {
                var text = "";
                if (OurPet != null)
                {
                    if (OurPet.EffectList != null)
                    {
                        var effectsParsed = new List<string>();
                        foreach (var currentEffect in OurPet.EffectList)
                        {
                            effectsParsed.Add(UserInterfaceHelper.ParseCurrentEffectToString(currentEffect));
                        }
                        return string.Join("\r\n", effectsParsed);
                    }
                }
                return text;
            }
        }



        // public (wszedzie) internal (w obrebie projektu *.dll) protected private  
        private System.Windows.Threading.DispatcherTimer dispatcherTimer;

        private static object rootSync = new object();
        public MainVM()
        {
            OurPet = new Hamster()
            {
                Name = "Stresik",
                Hunger = 1,
                //Tiredness = 1,
                EffectList = new List<CurrentEffect>()
            };
            MaxHealth = OurPet.Health;



            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            GameEngine.UpdateHealth(OurPet);
            GameEngine.UpdateTirednessAndHunger(OurPet);
            var isEndGame = GameEngine.CheckIfGameIsOver(OurPet);
            if (isEndGame)
            {
                dispatcherTimer.Stop();
                MessageBox.Show("Game over! Your pet is dead!");
            }
            NotifyPropertyChanged("Health");
            NotifyPropertyChanged("Hunger");
            NotifyPropertyChanged("PetDetails");
        }


        private ICommand feedWithCarrot;
        public ICommand FeedWithCarrot
        {
            get
            {
                if (feedWithCarrot == null) feedWithCarrot = new DelegateCommand(feedWithCarrotExecute, (x) => { return true; });
                return feedWithCarrot;
            }
        }

        private void feedWithCarrotExecute(object obj)
        {
            TextStatus += "\r\nFeed with carrot!";
            OurPet.EffectList.Add(OurPet.FeedingBehavior.Feed(new Food() { Type = Enum.FoodTypeEnum.CARROT }));
        }

        private ICommand feedWithGrass;
        public ICommand FeedWithGrass
        {
            get
            {
                if (feedWithGrass == null) feedWithGrass = new DelegateCommand(feedWithGrassExecute, (x) => { return true; });
                return feedWithGrass;
            }
        }

        private void feedWithGrassExecute(object obj)
        {
            TextStatus += "\r\nFeed with grass!";
            OurPet.EffectList.Add(OurPet.FeedingBehavior.Feed(new Food() { Type = Enum.FoodTypeEnum.GRASS }));
        }

        private static MainVM instance;
        public static MainVM Instance
        {
            get
            {
                if (instance != null) return instance;
                lock (rootSync)
                {
                    if (instance == null) instance = new MainVM();
                    return instance;
                }
            }
        }

    }
}
