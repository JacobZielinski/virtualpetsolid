﻿using Clean_Client.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Models
{
    public abstract class Creature
    {
        public List<CurrentEffect> EffectList { get; set; }
        public IFeedingBehavior FeedingBehavior { get; set; }

        public int AgeInDays { get; set; }
        public string Name { get; set; }
        public int Health { get; set; }
        public int Tiredness { get; set; }
        public int Hunger { get; set; }

        //public int getAgeInYears()
        //{
        //    return (AgeInDays / 365) + 1;
        //}

        public int AgeInYears
        {
            get
            {
                return (AgeInDays / 365) + 1;
            }
        }
        public bool IsAlive
        {
            get
            {
                return Health > 0;
            }
        }

    }
}
