﻿using Clean_Client.Behaviors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Models
{
    public class Hamster : Creature
    {
        public Hamster()
        {
            this.Health = 200;
            this.FeedingBehavior = new HamsterFeedingBehavior();
        }
    }
}
