﻿using Clean_Client.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Models
{
    public class CurrentEffect
    {
        public int CurrentIteration { get; set; }
        public EffectTypeEnum EffectType { get; set; }
        public int Duration { get; set; }
        public int Intensity { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
