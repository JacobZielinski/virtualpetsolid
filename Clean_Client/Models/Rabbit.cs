﻿using Clean_Client.Behaviors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Models
{
    public class Rabbit : Creature
    {
        public Rabbit()
        {
            this.Health = 100;
            this.FeedingBehavior = new RabbitFeedingBehavior();
        }
    }
}
