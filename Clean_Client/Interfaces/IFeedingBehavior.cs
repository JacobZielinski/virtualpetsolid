﻿using Clean_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clean_Client.Interfaces
{
    public interface IFeedingBehavior
    {
        CurrentEffect Feed(Food currentFood);
    }
}
